package ts;

import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.spark.SparkConf;
import org.apache.spark.TaskContext;
import org.apache.spark.api.java.*;
import org.apache.spark.api.java.function.*;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.*;
import org.apache.spark.streaming.kafka010.*;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.codehaus.jettison.json.JSONObject;
import scala.Tuple2;

import static org.apache.commons.lang3.StringUtils.SPACE;


public class TwitterKafkaStream {

    public static void main(String[] args) {
        // Create a local StreamingContext with two working thread and batch interval of 1 second
        SparkConf conf = new SparkConf().setMaster("local[2]").setAppName("StreamingTwitter");
        JavaStreamingContext jssc = new JavaStreamingContext(conf, Durations.seconds(60));
        SparkSession sparkSession = SparkSession.builder().config(conf).getOrCreate();
        Map<String, Object> kafkaParams = new HashMap<>();
        kafkaParams.put("bootstrap.servers", "worker1.sky.net:9092,worker2.sky.net:9092,master2.sky.net:9092");
        kafkaParams.put("key.deserializer", StringDeserializer.class);
        kafkaParams.put("value.deserializer", StringDeserializer.class);
        kafkaParams.put("group.id", "StreamingTwitter");
        kafkaParams.put("auto.offset.reset", "earliest");
        kafkaParams.put("enable.auto.commit", true);

        Collection<String> topics = Arrays.asList("IN");

        JavaInputDStream<ConsumerRecord<String, String>> stream =
                KafkaUtils.createDirectStream(
                        jssc,
                        LocationStrategies.PreferConsistent(),
                        ConsumerStrategies.Subscribe(topics, kafkaParams)
                );

        stream.foreachRDD(rdd -> {
            System.out.println("foreachRDD: " + rdd.count());
            if (!rdd.isEmpty()) {
                //Dataset<Row> row = sparkSession.createDataFrame(rdd, TwitterEntry.class);
                JavaRDD<String> entryRDD = rdd.map(record -> {
                   return record.value();
                });
                Dataset<Row> row = sparkSession.read().json(entryRDD);

                row.toDF().write().mode("append").parquet("/tmp/parquet/" + getDateString(new Date()));
            }
        });

        jssc.start();
        try {
            jssc.awaitTermination();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static String getDateString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String time = sdf.format(date);
        return time;
    }


}

